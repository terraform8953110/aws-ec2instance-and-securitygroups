# AWS EC2 Security Group Terraform Module
# Security Group for Private EC2 Instances
module "private_sg" {
  source  = "terraform-aws-modules/security-group/aws" # Using a well-maintained AWS Security Group module
  version = "5.1.1"                                    # Version pinning for predictability

  name        = "private_sg"                                                        # Descriptive name for the security group
  description = "Security Group allowing HTTP & SSH port open for entire VPC Block" # Clear description of the security group's purpose

  vpc_id = module.vpc.vpc_id # Utilizing outputs from other modules for consistency and maintainability

  # Ingress Rules & CIDR Blocks
  ingress_rules       = ["ssh-tcp", "http-80-tcp"]  # Allow SSH and HTTP access, consider adding more specific rules if needed
  ingress_cidr_blocks = [module.vpc.vpc_cidr_block] # Open to all IPv4 addresses within the VPC, consider restricting to specific IP ranges for better security

  # Egress Rule - All traffic allowed (Consider restricting egress traffic to specific ports/destinations for better security)
  egress_rules = ["all-all"]

  tags = local.common_tags # Utilizing local variables for consistent tagging across resources
}
