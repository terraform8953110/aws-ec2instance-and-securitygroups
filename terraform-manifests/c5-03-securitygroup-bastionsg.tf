module "public_bastion_sg" {
  source  = "terraform-aws-modules/security-group/aws" # Using a well-maintained AWS Security Group module
  version = "5.1.1"                                    # Version pinning for predictability

  name        = "public-bastion-sg"                                                                   # Descriptive name for the security group
  description = "Security Group allowing SSH access from any IPv4 CIDR block, with open egress ports" # Clear description of the security group's purpose

  vpc_id = module.vpc.vpc_id # Utilizing outputs from other modules for consistency and maintainability

  # Ingress Rules & CIDR Blocks
  ingress_rules       = ["ssh-tcp"]   # Only allowing SSH access, consider adding more specific rules if needed
  ingress_cidr_blocks = ["0.0.0.0/0"] # Open to all IPv4 addresses, consider restricting to specific IP ranges for better security

  # Egress Rule - All traffic allowed (Consider restricting egress traffic to specific ports/destinations for better security)
  egress_rules = ["all-all"]

  tags = local.common_tags # Utilizing local variables for consistent tagging across resources
}
