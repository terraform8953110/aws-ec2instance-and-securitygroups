# Output for public_bastion_sg security group

output "public_bastion_sg_group_id" {
  description = "The ID of the public_bastion_sg security group"
  value       = module.public_bastion_sg.security_group_id
}

output "public_bastion_sg_group_vpc_id" {
  description = "The VPC ID of the public_bastion_sg security group"
  value       = module.public_bastion_sg.security_group_vpc_id
}

output "public_bastion_sg_group_name" {
  description = "The name of the public_bastion_sg security group"
  value       = module.public_bastion_sg.security_group_name
}

# Output for private_sg security group

output "private_sg_group_id" {
  description = "The ID of the private_sg security group"
  value       = module.private_sg.security_group_id
}

output "private_sg_group_vpc_id" {
  description = "The VPC ID of the private_sg security group"
  value       = module.private_sg.security_group_vpc_id
}

output "private_sg_group_name" {
  description = "The name of the private_sg security group"
  value       = module.private_sg.security_group_name
}
