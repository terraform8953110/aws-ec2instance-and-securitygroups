# AWS EC2 Instance Terraform Module
# EC2 Instances that will be created in VPC Private Subnets
module "ec2_private" {
  depends_on = [module.vpc]                             # Ensure the VPC module is created first
  source     = "terraform-aws-modules/ec2-instance/aws" # Use the EC2 instance module from the Terraform registry
  version    = "5.6.1"                                  # Version pinning for predictability

  # Basic instance configuration
  name          = "${var.environment}-vm"                # Name for the EC2 instances
  ami           = data.aws_ami.amzlinux2.id              # AMI ID for the instances
  instance_type = var.instance_type                      # Instance type (e.g., t2.micro)
  key_name      = var.instance_keypair                   # SSH key pair for accessing the instances
  user_data     = file("${path.module}/app1-install.sh") # User data script for instance initialization
  tags          = local.common_tags                      # Tags to be applied to the instances

  # Security group and subnet configuration
  vpc_security_group_ids = [module.private_sg.security_group_id]                   # Security group IDs for the instances
  for_each               = toset(["0", "1"])                                       # Create one instance in each of the first two private subnets
  subnet_id              = element(module.vpc.private_subnets, tonumber(each.key)) # Subnet ID for each instance
}
