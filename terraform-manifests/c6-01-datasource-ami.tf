# Get latest AMI ID for Amazon Linux 2 OS
data "aws_ami" "amzlinux2" {
  most_recent = true       # Retrieve the most recent AMI
  owners      = ["amazon"] # Limit search to AMIs owned by Amazon

  # Filter criteria to narrow down the search
  filter {
    name   = "name"                  # Filter by AMI name
    values = ["amzn2-ami-hvm-*-gp2"] # Specify the name pattern of the AMI
  }
  filter {
    name   = "root-device-type" # Filter by root device type (EBS)
    values = ["ebs"]
  }
  filter {
    name   = "virtualization-type" # Filter by virtualization type (HVM)
    values = ["hvm"]
  }
  filter {
    name   = "architecture" # Filter by architecture (x86_64)
    values = ["x86_64"]
  }
}
